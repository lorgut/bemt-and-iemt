#!/usr/bin/Rscript

source('../common.r')

bemt <- readThresholdData('BEMT_', 'SnifferCongestionExecutions/BEMT/BEMTSnifferCongestion.csv')
iemt <- readThresholdData('IEMT_', 'SnifferCongestionExecutions/IEMT/IEMTSnifferCongestion.csv')
emt <- readMutantPercentage('EMT', 'SnifferCongestionExecutions/EMT/MutantPercentageSnifferCongestion.txt')
comparisons <- runExperiment('.', rbind(bemt, iemt, emt))

casesVariants <- casesComparisonBetweenVariants(comparisons)
casesBEMT_EMT <- casesComparisonBetweenVariantAndEMT('BEMT_', comparisons)
casesIEMT_EMT <- casesComparisonBetweenVariantAndEMT('IEMT_', comparisons)

write.csv(casesVariants, 'casesVariants.csv')
write.csv(casesBEMT_EMT, 'casesBEMT_EMT.csv')
write.csv(casesIEMT_EMT, 'casesIEMT_EMT.csv')
