#!/usr/bin/Rscript

source('../common.r')

bemt <- readThresholdData('BEMT_', 'BruteForceExecutions/BEMT/BEMTBruteForce.csv')
iemt <- readThresholdData('IEMT_', 'BruteForceExecutions/IEMT/IEMTBruteForce.csv')
emt <- readMutantPercentage('EMT', 'BruteForceExecutions/EMT/MutantPercentageBruteForce.txt')
comparisons <- runExperiment('.', rbind(bemt, iemt, emt))

casesVariants <- casesComparisonBetweenVariants(comparisons)
casesBEMT_EMT <- casesComparisonBetweenVariantAndEMT('BEMT_', comparisons)
casesIEMT_EMT <- casesComparisonBetweenVariantAndEMT('IEMT_', comparisons)

write.csv(casesVariants, 'casesVariants.csv')
write.csv(casesBEMT_EMT, 'casesBEMT_EMT.csv')
write.csv(casesIEMT_EMT, 'casesIEMT_EMT.csv')

## STUDY ON EXCLUDING RRO FOR BRUTE FORCE ##

bemtNoRRO <- readThresholdData('BEMT_', 'BruteForceExecutions/NoRRO/BEMT/NoRROBEMTBruteForce.csv')
iemtNoRRO <- readThresholdData('IEMT_', 'BruteForceExecutions/NoRRO/IEMT/NoRROIEMTBruteForce.csv')
emtNoRRO <- readMutantPercentage(
  tech = 'EMT',
  file = 'BruteForceExecutions/NoRRO/EMT/MutantPercentageBruteForce.txt'
)
comparisonsNoRRO <- runExperiment('.', rbind(bemtNoRRO, iemtNoRRO, emtNoRRO), suffix='-noRRO')

casesVariantsNoRRO <- casesComparisonBetweenVariants(comparisonsNoRRO)
casesBEMT_EMTNoRRO <- casesComparisonBetweenVariantAndEMT('BEMT_', comparisonsNoRRO)
casesIEMT_EMTNoRRO <- casesComparisonBetweenVariantAndEMT('IEMT_', comparisonsNoRRO)

write.csv(casesVariantsNoRRO, 'casesVariants-NoRRO.csv')
write.csv(casesBEMT_EMTNoRRO, 'casesBEMT_EMT-NoRRO.csv')
write.csv(casesIEMT_EMTNoRRO, 'casesIEMT_EMT-NoRRO.csv')
