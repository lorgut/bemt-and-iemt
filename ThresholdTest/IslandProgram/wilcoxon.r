#!/usr/bin/Rscript

source('../common.r')

bemt <- readThresholdData('BEMT_', 'IslandExecutions/BEMT/BEMTIsland.csv')
iemt <- readThresholdData('IEMT_', 'IslandExecutions/IEMT/IEMTIsland.csv')
emt <- readMutantPercentage('EMT', 'IslandExecutions/EMT/MutantPercentageIsland.txt')
comparisons <- runExperiment('.', rbind(bemt, iemt, emt))

casesVariants <- casesComparisonBetweenVariants(comparisons)
casesBEMT_EMT <- casesComparisonBetweenVariantAndEMT('BEMT_', comparisons)
casesIEMT_EMT <- casesComparisonBetweenVariantAndEMT('IEMT_', comparisons)

write.csv(casesVariants, 'casesVariants.csv')
write.csv(casesBEMT_EMT, 'casesBEMT_EMT.csv')
write.csv(casesIEMT_EMT, 'casesIEMT_EMT.csv')
