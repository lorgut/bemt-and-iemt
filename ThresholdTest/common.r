library(data.table)
library(stringr)

epsilon <- 0.05

readThresholdData <- function (prefix, csvFile) {
  wide <- read.csv(csvFile,
                   sep=' ', header=FALSE,
                   col.names=c('name', paste0(prefix, c('05', '10', '15', '20', '25', '30', '35', '40'))))
  
  wideParts <- str_split_fixed(as.character(wide$name),'-',3)
  wide$strong <- as.double(wideParts[,2])
  wide$seed <- wideParts[,3]
  wide <- subset(wide, select=-c(name))
  
  # Returns data in long format
  melt(setDT(wide), id.vars=c('strong', 'seed'), variable.name='tech')
}

readMutantPercentage <- function(tech, file) {
  data <- read.table(text = gsub('-', ' ', readLines(file)),
                     header=FALSE,
                     col.names = c('dummy', 'strong', 'seed', 'value')
  )
  data <- subset(data, select=-c(dummy))
  data$tech <- tech
  data
}

compareResults <- function(facts, techLeft, techRight) {
  strongValues <- unique(facts$strong)
  
  results <- data.frame(
    strong=strongValues,
    techLeft=techLeft,
    techRight=techRight
  )
  for (strongValue in strongValues) {
    factsLeft <- subset(facts, strong==strongValue & tech==techLeft)$value
    factsRight <- subset(facts, strong==strongValue & tech==techRight)$value
    results[results$strong==strongValue, 'p.value.twosided'] <- wilcox.test(factsLeft, factsRight, paired=FALSE, exact=FALSE)$p.value
    results[results$strong==strongValue, 'p.value.less'] <- wilcox.test(factsLeft, factsRight, paired=FALSE, exact=FALSE, alternative='less')$p.value
    results[results$strong==strongValue, 'p.value.greater'] <- wilcox.test(factsLeft, factsRight, paired=FALSE, exact=FALSE, alternative = 'greater')$p.value
  }
  
  results
}

runExperiment <- function(basedir, facts, suffix='') {
  write.csv(facts, file=paste(sep='/', basedir, paste0('facts', suffix, '.csv')))
  
  stats <- do.call(data.frame, aggregate(value ~ strong + tech, data=facts, function(x) {
    c(Mean=mean(x),
      min=min(x),
      max=max(x),
      Median=median(x),
      SD=sd(x))
  }))
  write.csv(stats, file=paste(sep='/', basedir, paste0('stats', suffix, '.csv')))
  
  options <- list(
    list(left='IEMT_05', right='BEMT_05'),
    list(left='IEMT_10', right='BEMT_10'),
    list(left='IEMT_15', right='BEMT_15'),
    list(left='IEMT_20', right='BEMT_20'),
    list(left='IEMT_25', right='BEMT_25'),
    list(left='IEMT_30', right='BEMT_30'),
    list(left='IEMT_35', right='BEMT_35'),
    list(left='IEMT_40', right='BEMT_40'),

    list(left='IEMT_05', right='EMT'),
    list(left='IEMT_10', right='EMT'),
    list(left='IEMT_15', right='EMT'),
    list(left='IEMT_20', right='EMT'),
    list(left='IEMT_25', right='EMT'),
    list(left='IEMT_30', right='EMT'),
    list(left='IEMT_35', right='EMT'),
    list(left='IEMT_40', right='EMT'),

    list(left='BEMT_05', right='EMT'),
    list(left='BEMT_10', right='EMT'),
    list(left='BEMT_15', right='EMT'),
    list(left='BEMT_20', right='EMT'),
    list(left='BEMT_25', right='EMT'),
    list(left='BEMT_30', right='EMT'),
    list(left='BEMT_35', right='EMT'),
    list(left='BEMT_40', right='EMT')
  )

  comparisons <- data.frame()
  for (option in options) {
    comparisons <- rbind(comparisons, compareResults(facts, option$left, option$right))
  }
  write.csv(comparisons, file=paste(sep='/', basedir, paste0('comparisons', suffix, '.csv')))

  comparisons
}

casesComparisonBetweenVariants <- function(comparisons) {
  choiceByStrong <- data.frame()
  for (strongValue in unique(comparisons$strong)) {
    rows <- subset(comparisons, comparisons$strong == strongValue & comparisons$techRight != 'EMT')
    
    choiceByStrong <- rbind(choiceByStrong, data.frame(
      strong = strongValue,
      bemtBetter = sum(rows$p.value.twosided < epsilon & rows$p.value.greater < epsilon),
      same = sum(rows$p.value.twosided >= epsilon),
      iemtBetter = sum(rows$p.value.twosided < epsilon & rows$p.value.less < epsilon)
    ))
  }

  choiceByStrong
}

casesComparisonBetweenVariantAndEMT <- function(prefixLeft, comparisons) {
  choiceByStrong <- data.frame()
  for (strongValue in unique(comparisons$strong)) {
    rows <- subset(comparisons, comparisons$strong == strongValue
                   & startsWith(as.character(comparisons$techLeft), prefixLeft)
                   & comparisons$techRight == 'EMT')
    
    choiceByStrong <- rbind(choiceByStrong, data.frame(
      strong = strongValue,
      emtBetter = sum(rows$p.value.twosided < epsilon & rows$p.value.greater < epsilon),
      same = sum(rows$p.value.twosided >= epsilon),
      variantBetter = sum(rows$p.value.twosided < epsilon & rows$p.value.less < epsilon)
    ))
  }
  
  choiceByStrong
}