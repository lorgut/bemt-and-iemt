hof-0.30-acknowledgements 31.66
hof-0.30-administratively 34.16
hof-0.30-Australopithecus 34.16
hof-0.30-bloodthirstiness 35.00
hof-0.30-cinematographers 40.83
hof-0.30-counterespionage 31.66
hof-0.30-Czechoslovakians 33.33
hof-0.30-departmentalized 32.50
hof-0.30-disqualification 33.33
hof-0.30-enthusiastically 35.00
hof-0.30-flibbertigibbets 32.50
hof-0.30-gastrointestinal 27.50
hof-0.30-hospitalizations 41.66
hof-0.30-hydroelectricity 32.50
hof-0.30-intellectualized 28.33
hof-0.30-lightheartedness 27.50
hof-0.30-melodramatically 26.66
hof-0.30-multimillionaire 34.16
hof-0.30-nationalizations 32.50
hof-0.30-neurotransmitter 32.50
hof-0.30-overcompensating 28.33
hof-0.30-photojournalists 35.00
hof-0.30-predetermination 33.33
hof-0.30-rambunctiousness 33.33
hof-0.30-sentimentalizing 35.83
hof-0.30-synchronizations 32.50
hof-0.30-teleconferencing 34.16
hof-0.30-transcendentally 33.33
hof-0.30-unscrupulousness 34.16
hof-0.30-whatchamacallits 34.16
hof-0.45-acknowledgements 46.66
hof-0.45-administratively 49.16
hof-0.45-Australopithecus 42.50
hof-0.45-bloodthirstiness 49.16
hof-0.45-cinematographers 55.00
hof-0.45-counterespionage 45.83
hof-0.45-Czechoslovakians 49.16
hof-0.45-departmentalized 47.50
hof-0.45-disqualification 48.33
hof-0.45-enthusiastically 48.33
hof-0.45-flibbertigibbets 47.50
hof-0.45-gastrointestinal 47.50
hof-0.45-hospitalizations 49.16
hof-0.45-hydroelectricity 47.50
hof-0.45-intellectualized 50.00
hof-0.45-lightheartedness 42.50
hof-0.45-melodramatically 42.50
hof-0.45-multimillionaire 49.16
hof-0.45-nationalizations 48.33
hof-0.45-neurotransmitter 40.83
hof-0.45-overcompensating 42.50
hof-0.45-photojournalists 41.66
hof-0.45-predetermination 48.33
hof-0.45-rambunctiousness 48.33
hof-0.45-sentimentalizing 49.16
hof-0.45-synchronizations 47.50
hof-0.45-teleconferencing 49.16
hof-0.45-transcendentally 49.16
hof-0.45-unscrupulousness 55.00
hof-0.45-whatchamacallits 50.00
hof-0.60-acknowledgements 62.50
hof-0.60-administratively 64.16
hof-0.60-Australopithecus 58.33
hof-0.60-bloodthirstiness 57.50
hof-0.60-cinematographers 62.50
hof-0.60-counterespionage 61.66
hof-0.60-Czechoslovakians 65.00
hof-0.60-departmentalized 62.50
hof-0.60-disqualification 64.16
hof-0.60-enthusiastically 68.33
hof-0.60-flibbertigibbets 63.33
hof-0.60-gastrointestinal 62.50
hof-0.60-hospitalizations 64.16
hof-0.60-hydroelectricity 63.33
hof-0.60-intellectualized 65.83
hof-0.60-lightheartedness 58.33
hof-0.60-melodramatically 55.83
hof-0.60-multimillionaire 65.83
hof-0.60-nationalizations 69.16
hof-0.60-neurotransmitter 55.83
hof-0.60-overcompensating 57.50
hof-0.60-photojournalists 55.83
hof-0.60-predetermination 56.66
hof-0.60-rambunctiousness 62.50
hof-0.60-sentimentalizing 63.33
hof-0.60-synchronizations 54.16
hof-0.60-teleconferencing 63.33
hof-0.60-transcendentally 70.83
hof-0.60-unscrupulousness 70.83
hof-0.60-whatchamacallits 65.00
hof-0.75-acknowledgements 78.33
hof-0.75-administratively 78.33
hof-0.75-Australopithecus 80.00
hof-0.75-bloodthirstiness 72.50
hof-0.75-cinematographers 77.50
hof-0.75-counterespionage 70.00
hof-0.75-Czechoslovakians 78.33
hof-0.75-departmentalized 85.83
hof-0.75-disqualification 78.33
hof-0.75-enthusiastically 84.16
hof-0.75-flibbertigibbets 78.33
hof-0.75-gastrointestinal 79.16
hof-0.75-hospitalizations 78.33
hof-0.75-hydroelectricity 75.83
hof-0.75-intellectualized 80.83
hof-0.75-lightheartedness 78.33
hof-0.75-melodramatically 69.16
hof-0.75-multimillionaire 80.00
hof-0.75-nationalizations 85.00
hof-0.75-neurotransmitter 77.50
hof-0.75-overcompensating 73.33
hof-0.75-photojournalists 71.66
hof-0.75-predetermination 71.66
hof-0.75-rambunctiousness 75.83
hof-0.75-sentimentalizing 77.50
hof-0.75-synchronizations 70.00
hof-0.75-teleconferencing 78.33
hof-0.75-transcendentally 85.83
hof-0.75-unscrupulousness 77.50
hof-0.75-whatchamacallits 76.66
hof-0.90-acknowledgements 93.33
hof-0.90-administratively 93.33
hof-0.90-Australopithecus 88.33
hof-0.90-bloodthirstiness 93.33
hof-0.90-cinematographers 92.50
hof-0.90-counterespionage 86.66
hof-0.90-Czechoslovakians 93.33
hof-0.90-departmentalized 94.16
hof-0.90-disqualification 91.66
hof-0.90-enthusiastically 92.50
hof-0.90-flibbertigibbets 93.33
hof-0.90-gastrointestinal 94.16
hof-0.90-hospitalizations 92.50
hof-0.90-hydroelectricity 91.66
hof-0.90-intellectualized 91.66
hof-0.90-lightheartedness 91.66
hof-0.90-melodramatically 92.50
hof-0.90-multimillionaire 93.33
hof-0.90-nationalizations 91.66
hof-0.90-neurotransmitter 92.50
hof-0.90-overcompensating 94.16
hof-0.90-photojournalists 92.50
hof-0.90-predetermination 93.33
hof-0.90-rambunctiousness 92.50
hof-0.90-sentimentalizing 92.50
hof-0.90-synchronizations 91.66
hof-0.90-teleconferencing 94.16
hof-0.90-transcendentally 92.50
hof-0.90-unscrupulousness 91.66
hof-0.90-whatchamacallits 92.50
