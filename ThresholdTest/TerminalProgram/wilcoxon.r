#!/usr/bin/Rscript

source('../common.r')

bemt <- readThresholdData('BEMT_', 'TerminalExecutions/BEMT/BEMTTerminal.csv')
iemt <- readThresholdData('IEMT_', 'TerminalExecutions/IEMT/IEMTTerminal.csv')
emt <- readMutantPercentage('EMT', 'TerminalExecutions/EMT/MutantPercentageTerminal.txt')
comparisons <- runExperiment('.', rbind(bemt, iemt, emt))

casesVariants <- casesComparisonBetweenVariants(comparisons)
casesBEMT_EMT <- casesComparisonBetweenVariantAndEMT('BEMT_', comparisons)
casesIEMT_EMT <- casesComparisonBetweenVariantAndEMT('IEMT_', comparisons)

write.csv(casesVariants, 'casesVariants.csv')
write.csv(casesBEMT_EMT, 'casesBEMT_EMT.csv')
write.csv(casesIEMT_EMT, 'casesIEMT_EMT.csv')
